package com.mastertech.controletarefas.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastertech.controletarefas.persistence.Project;
import com.mastertech.controletarefas.persistence.ProjectRepository;
import com.mastertech.controletarefas.persistence.Task;
import com.mastertech.controletarefas.persistence.TaskRepository;
import com.mastertech.controletarefas.web.dto.TaskPayload;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class ITTaskController {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private MockMvc mockMvc;

    private Project createdProject;
    private Task createdTask;

    @BeforeEach
    public void prepare(){
        Project project = new Project();
        project.setName("Blog");
        createdProject = projectRepository.save(project);

        Task task = new Task();
        task.setName("Criar post");
        task.setStatus("To Do");
        task.setProject(createdProject);

        createdTask = taskRepository.save(task);
    }

    @Test
    public void shouldListAllProjectTasks() throws Throwable {
        mockMvc.perform(MockMvcRequestBuilders
                    .get("/projeto/" + createdProject.getId() +"/tarefa"))
                .andExpect(MockMvcResultMatchers
                    .jsonPath("[0].id")
                    .value(createdTask.getId()));
    }

    @Test
    public void shouldCreateATask() throws Throwable{
        TaskPayload payload = new TaskPayload();
        payload.setName("Criar mais um post");
        payload.setStatus("To Do");

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(payload);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/projeto/" + createdProject.getId() +"/tarefa")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("id")
                        .value(createdProject.getId() + 1));
    }
}
